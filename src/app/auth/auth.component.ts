import { Component, OnInit } from '@angular/core';
import { AuthServices } from '../services/auth.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  authStatus: boolean;

  constructor(private authServices: AuthServices, private router: Router) { }

  ngOnInit() {
    this.authStatus = this.authServices.isAuth;
  }

  onSignIn() {
    this.authServices.signIn().then(
      () => {
        console.log('Sign in successful!');
        this.authStatus = this.authServices.isAuth;
        this.router.navigate(['appareils']);
      }
    );
  }

  onSignOut() {
    this.authServices.signOut();
    this.authStatus = this.authServices.isAuth;
  }


}

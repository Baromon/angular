import { Component, OnInit } from '@angular/core';
import { AppareilServices } from '../services/appareil.services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-appareil',
  templateUrl: './single-appareil.component.html',
  styleUrls: ['./single-appareil.component.scss']
})

export class SingleAppareilComponent implements OnInit {

  name: string = 'Appareil';
  status: string = 'Statut';
  

  constructor(private appareilServices: AppareilServices,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.name = this.appareilServices.getAppareilById(+id).name;
    this.status = this.appareilServices.getAppareilById(+id).status;
  }

}
